package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class OneElementCasesSortingTest {
    private final String[] testArray;
    private final String[] expectedArray;


    public OneElementCasesSortingTest(String[] testArray, String[] expectedArray) {
        this.testArray = testArray;
        this.expectedArray = expectedArray;
    }

    /**
     * Testing the case when only one element exists in the array
     */
    @Test
    public void oneElementCaseTest() {
        Main.main(testArray);
        Assert.assertArrayEquals(expectedArray, testArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> values() {
        return Arrays.asList(new Object[][]{
                {new String[]{"-1"}, new String[]{"-1"}},
                {new String[]{"0"}, new String[]{"0"}},
                {new String[]{"2"}, new String[]{"2"}},
                {new String[]{"20"}, new String[]{"20"}},
        });
    }

}