package com.example;

import org.junit.Test;

public class BadCasesSortingTest {

    /**
     * Testing the case when null was passed as an argument.
     */
    @Test(expected = IllegalArgumentException.class)
    public void nullArgCase() {
        Main.main(null);
    }

    /**
     * Testing the case when the array's length is 0.
     */
    @Test(expected = IllegalArgumentException.class)
    public void emptyArgCase() {
        Main.main(new String[]{});
    }

    /**
     * Testing the case when the array contains more than 10 elements.
     */
    @Test(expected = IllegalArgumentException.class)
    public void moreThan10ArgCase() {
        Main.main(new String[]{"10", "1", "5", "-4", "-80", "10", "8", "2", "-2", "17", "1"});
    }

    /**
     * Testing the case when one of array's element is not a digit.
     */

    @Test(expected = IllegalArgumentException.class)
    public void noDigitCase() {
        Main.main(new String[]{"a", "8", "2", "-2", "17", "1"});
    }

    /**
     * Testing the case when at least one element is double.
     */
    @Test(expected = IllegalArgumentException.class)
    public void noIntegerCase() {
        Main.main(new String[]{"1", "1.2", "5.8", "-4"});
    }
}
