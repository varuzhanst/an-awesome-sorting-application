package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class NormalCasesSortingTest {
    private final String[] testArray;
    private final String[] expectedArray;


    public NormalCasesSortingTest(String[] testArray, String[] expectedArray) {
        this.testArray = testArray;
        this.expectedArray = expectedArray;
    }

    /**
     * Testing the case when the passed array have more than 0 and less than 10 elements and all of them are integers.
     */
    @Test
    public void oneElementCaseTest() {
        Main.main(testArray);
        Assert.assertArrayEquals(expectedArray, testArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> values() {
        return Arrays.asList(new Object[][]{
                {new String[]{"-1", "0"}, new String[]{"-1", "0"}},
                {new String[]{"0", "-8", "5"}, new String[]{"-8", "0", "5"}},
                {new String[]{"2", "-10", "1", "-10"}, new String[]{"-10", "-10", "1", "2"}},
                {new String[]{"100", "200", "50", "0", "-50"}, new String[]{"-50", "0", "50", "100", "200"}},
        });
    }

}