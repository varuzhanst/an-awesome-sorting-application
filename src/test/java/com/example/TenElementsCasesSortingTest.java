package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TenElementsCasesSortingTest {
    private final String[] testArray;
    private final String[] expectedArray;


    public TenElementsCasesSortingTest(String[] testArray, String[] expectedArray) {
        this.testArray = testArray;
        this.expectedArray = expectedArray;
    }

    /**
     * Testing the case when 10 elements are passed in the array.
     */
    @Test
    public void oneElementCaseTest() {
        Main.main(testArray);
        Assert.assertArrayEquals(expectedArray, testArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> values() {
        return Arrays.asList(new Object[][]{
                {new String[]{"-1", "-2", "-3", "-4", "0", "4", "3", "2", "1", "0"}, new String[]{"-4", "-3", "-2", "-1", "0", "0", "1", "2", "3", "4"}},
                {new String[]{"-1", "-2", "3", "-4", "0", "4", "-3", "2", "1", "0"}, new String[]{"-4", "-3", "-2", "-1", "0", "0", "1", "2", "3", "4"}},
                {new String[]{"1", "2", "3", "-4", "0", "4", "-3", "-2", "-1", "0"}, new String[]{"-4", "-3", "-2", "-1", "0", "0", "1", "2", "3", "4"}},
                {new String[]{"1", "-2", "-3", "-4", "0", "4", "3", "2", "-1", "0"}, new String[]{"-4", "-3", "-2", "-1", "0", "0", "1", "2", "3", "4"}}
        });
    }

}