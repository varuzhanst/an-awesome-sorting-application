package com.example;

public class Sorting {
    /**
     * @param array is expected as an array of integer elements in String's array.
     *              This method sorts the passed array using Bubble sorting algorithm.
     */
    static void sort(String[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                try {
                    if (Integer.parseInt(array[j]) > Integer.parseInt(array[j + 1])) {
                        int temp = Integer.parseInt(array[j]);
                        array[j] = array[j + 1];
                        array[j + 1] = Integer.toString(temp);
                    }
                } catch (Exception e) {
                    throw new IllegalArgumentException("Only integer is expected.");
                }

            }

        }
    }
}
