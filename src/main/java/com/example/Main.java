package com.example;

import java.util.Arrays;

public class Main {
    /**
     * @param args accepts array of integers in String format from command line as an argument.The maximum count of elements in array should be 10.
     *             As a result the passed array is being sorted in ascending order and being printed in standard output.
     */
    public static void main(String[] args) {
        if (args == null)
            throw new IllegalArgumentException("Null was passed as an argument.");

        if (args.length == 0)
            throw new IllegalArgumentException("No arguments passed.");
        else if (args.length > 10)
            throw new IllegalArgumentException("More than 10 arguments passed.");

        Sorting.sort(args);

        System.out.println(Arrays.toString(args));
    }
}

